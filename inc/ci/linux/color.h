/*
 * CC+ interpreter written in C
 * Copyright (C) 2020 0v3rl0w & contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CPC_LINUX_COLOR
#define CPC_LINUX_COLOR

static char* colorsFore[16] = {
       "\033[30m", // black
       "\033[34m", // blue
       "\033[32m", // green
       "\033[36m", // cyan
       "\033[31m", // red
       "\033[35m", // magenta
       "\033[33m", // brown
       "\033[37m", // light grey
       "\033[90m", // dark grey
       "\033[94m", // light blue
       "\033[92m", // light green
       "\033[96m", // light cyan
       "\033[91m", // light red
       "\033[95m", // light magenta
       "\033[93m", // yellow
       "\033[39m" // white
};

int convertColor(char* colorCode);
void changeForeground(char* color);
void changeBackground(char* color);

#endif
