/*
 * CC+ interpreter written in C
 * Copyright (C) 2020 0v3rl0w & contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ci/linux/color.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int convertColor(char* colorCode){
    int colorValue = 0;

    if(strlen(colorCode) > 2) {
            printf("The color code %s is invalid !\n", colorCode);
            exit(1);
    }

    if(colorCode[0] < 48 || colorCode[0] > 57) {
        printf("Invalid carracter in color code: %c\n", colorCode[0]);
        exit(1);
    }
    if(strlen(colorCode) == 2) {
        if(colorCode[1] < 48 || colorCode[1] > 57) {
            printf("Invalid carracter in color codes : %c\n", colorCode[1]);
            exit(1);
        }
        colorValue = colorCode[1] - '0';
        colorValue += (colorCode[0] - '0') * 10;
    }

    else if(strlen(colorCode) == 1) {
        colorValue = colorCode[0] - '0';
    }

    if(colorValue > 15) {
        printf("The color code %s is invalid\n", colorCode);
        exit(1);
    }

    return colorValue;
}

void changeForeground(char* color) {
    printf("%s", colorsFore[convertColor(color)]);
}

void changeBackground(char* color) {
    char* colorCode = colorsFore[convertColor(color)];
    char* newCode = (char*) malloc(sizeof(char*) * strlen(colorCode));

    if(newCode == NULL) {
        printf("Cannot initiate the variable newCode from the function\nChangeBackground(char* color)\n");
        exit(1);
    }

    if(colorCode[2] == '9')
        sprintf(newCode, "\033[10%cm", colorCode[3]);
    else
        sprintf(newCode, "\033[4%cm", colorCode[3]);

    printf("%s", newCode);
    free(newCode);
}
