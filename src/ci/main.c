/*
 * CC+ interpreter written in C
 * Copyright (C) 2020 0v3rl0w & contributors
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ci/cpc/parser.h>
#include <ci/cpc/CPCLINE.h>

#include <stdio.h>
#include <stdlib.h>


int main(int argc, char* argv[]) {
    if(argc == 1 || argc > 2) {
        fprintf(stderr, "Usage: %s FILE.cpc\n", argv[0]);
        return 1;
    }

    CPCLINE file_lines = read_lines(argv[1]);
    parse(file_lines, argv[1]);

    free(file_lines.content);
}
