/*
 * CC+ interpreter written in C
 * Copyright (C) 2020 0v3rl0w & contributors
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ci/cpc/CPCVAR.h>
#include <ci/cpc/str.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void init_CPCVAR(CPCVAR* var) {
    var->length = 0;
    var->keys = malloc(sizeof(char*) * 2048);

    if(var->keys == NULL) {
        printf("Memory Error\n");
        printf("Cannot initiate the CPCVAR keys\n");
        exit(1);
    }

    var->values = malloc(sizeof(char*) * 2048);

    if(var->values == NULL) {
        printf("Memory Error\n");
        printf("Cannot initiate the CPCVAR values\n");
        free(var->keys);
        exit(1);
    }
}

void declare_var(CPCVAR* var, char* key, char* value) {
    var->keys[var->length] = key;
    var->values[var->length] = value;
    var->length++;
}

char* get_var(CPCVAR* var, char* varname) {
    char* key = strLower(varname);
    for(int i=0; i < var->length; i++) {
        if(strcmp(var->keys[i], key) == 0) {
            free(key);
            return var->values[i];
        }
    }

    printf("Error: Variable %s not declared !\n", key);
    free(key);
    exit(1);
}

CPCTOKEN place_var(CPCVAR* var, CPCTOKEN token) {
    CPCTOKEN new_token;
    char** tokens = malloc(sizeof(char*) * token.length);

    if(tokens == NULL) {
        printf("Memory Error\n");
        printf("Cannot initiate the tokens variable from the function\nplace_var(CPCVAR* var, CPCTOKEN token)\n");
        exit(1);
    }

    int length = 0;
    char* tmp;

    for(int i=0; i < token.length; i++) {
        char* tok = token.content[i];

        if(tok[0] == '%') {
            tmp = malloc(sizeof(char) * strlen(token.content[i]) - 2);

            if(tmp == NULL) {
                printf("Memory Error\n");
                printf("Cannot initiate the temporary variable from the function\nCPCTOKEN place_var(CPCVAR* var, CPCTOKEN token)\n");
                free(tokens);
                exit(1);
            }

            strncpy(tmp, token.content[i]+1, strlen(token.content[i]) -2 );
            tmp[strlen(token.content[i])-2] = '\0';
            tmp = get_var(var, tmp);
        } else {
            tmp = malloc(sizeof(char) * strlen(token.content[i]));

            if(tmp == NULL) {
                printf("Memory Error\n");
                printf("Cannot initiate the temporary variable from the function\nCPCTOKEN place_var(CPCVAR* var, CPCTOKEN token)\n");
                free(tokens);
                exit(1);
            }

            strcpy(tmp, token.content[i]);
        }

        tokens[length++] = tmp;
    }

    new_token.length = length;
    new_token.content = tokens;

    return new_token;
}

void destroy_CPCVAR(CPCVAR* var) {
    free(var->values);
    free(var->keys);
}

void merge_CPCVAR(CPCVAR* dst, CPCVAR src) {
    for(int i=0; i < src.length; i++) {
        declare_var(dst, src.keys[i], src.values[i]);
    }
}
