/*
 * CC+ interpreter written in C
 * Copyright (C) 2020 0v3rl0w & contributors
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifdef __unix__
#define CLEAR "clear"
#include <ci/linux/color.h>
#elif defined(_WIN32) || defined(WIN32)
#error THIS VERSION IS NOT MEANT TO RUN ON WINDOWS ! (Work in progress...)
#define CLEAR "cls"
#endif

#include <ci/cpc/filereader.h>
#include <ci/cpc/parser.h>
#include <ci/cpc/str.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printCPC(CPCTOKEN tokens) {
    char* last_token = tokens.content[tokens.length-1];
    char* lower = strLower(last_token);
    char* token;

    if(strcmp(lower, "/#r") == 0) {
        token = showTokens(tokens, 1, -1);
        printf("%s", token);
    } else {
        token = showTokens(tokens, 1, 0);
        printf("%s\n", token);
    }

    free(token);
    free(lower);
}

void execute(CPCTOKEN tokens, CPCVAR* var) {
    char* keyword = strLower(tokens.content[0]);
    char* completeLine = showTokens(tokens, 0, 0);

    if(strcmp(keyword, "txt/") == 0) {
        CPCTOKEN new_tokens = place_var(var, tokens);
        printCPC(new_tokens);
        destroy_CPCTOKEN(&new_tokens);
    }

    else if(strcmp(keyword, "exe/") == 0) {
        if(tokens.length == 1) {
            printf("Please provide a filename after exe/\n");
            free(keyword);
            free(completeLine);
            exit(1);
        }

        else if(tokens.length == 2) {
            if(strcmp(tokens.content[1], "&") == 0) {
                printf("Did you forget to put a filename ? because & is not a valide filename\n");
                free(keyword);
                free(completeLine);
                exit(1);
            }

            CPCLINE lines = read_lines(tokens.content[1]);
            parse(lines, tokens.content[1]);
            free(keyword);
            free(completeLine);
            exit(0);
        }

        else if(tokens.length == 3) {
            CPCLINE lines = read_lines(tokens.content[2]);

            if(strcmp(tokens.content[1], "&") == 0) {
                CPCVAR variables = parse_withvar(lines, tokens.content[2]);
                merge_CPCVAR(var, variables);
                destroy_CPCVAR(&variables);
                free(keyword);
                free(completeLine);
                return;
            }

            else {
                printf("Unknown execution mode !\n");
                free(keyword);
                free(completeLine);
                exit(1);
            }

        } else {
            printf("Too much argument for exe/\n");
            free(keyword);
            free(completeLine);
            exit(1);
        }
    }

    else if(strcmp(keyword, "cls/") == 0) {
        printf("\n");
        int return_code = system(CLEAR);

        if(return_code == 0) {
            printf("Shell not available !\n");
            free(keyword);
            free(completeLine);
            exit(1);
        }

        if(return_code == -1 || return_code == 127) {
            printf("Cannot spawn the clear command !\n");
            free(keyword);
            free(completeLine);
            exit(1);
        }
    }

    else if(strcmp(keyword, "colorf/") == 0 || strcmp(keyword, "couleurc/") == 0) {
        if(tokens.length != 2) {
            printf("\n[-] Syntax error on the line:\n%s\nToo much arguments.\n", completeLine);
            free(keyword);
            free(completeLine);
            exit(1);
        }

        changeForeground(tokens.content[1]);
    }

    else if(strcmp(keyword, "colorb/") == 0 || strcmp(keyword, "couleurf/") == 0) {
        if(tokens.length != 2) {
            printf("\n[-] Syntax error on the line:\n%s\nToo much arguments.\n", completeLine);
            free(keyword);
            free(completeLine);
            exit(1);
        }

        changeBackground(tokens.content[1]);
    }

    else if(strcmp(keyword, "fix/") == 0) {
        char* declaration = showTokens(tokens, 1, 0);
        char* varnameMalloc = malloc(sizeof(char) * strlen(declaration));


        if(varnameMalloc == NULL) {
            printf("Cannot initiate the varnameMalloc variable from the function\nexecute(CPCTOKEN tokens, CPCVAR* var)\n");
            free(keyword);
            free(completeLine);
            free(declaration);
            exit(1);
        }

        char* question = strLower(tokens.content[1]);

        if(strcmp(question, "/q") == 0) {
            if(tokens.length != 3) {
                printf("\n[-]Syntax error on the line:\n%s\nToo much arguments.\n", completeLine);
                free(keyword);
                free(varnameMalloc);
                free(declaration);
                free(completeLine);
                exit(1);
            }

            char buffer[1024];
            if(fgets(buffer, 1024, stdin) == NULL) {
                printf("Couldn't read the input !\n");
                free(keyword);
                free(varnameMalloc);
                free(declaration);
                free(completeLine);
                exit(1);
            }

            buffer[strcspn(buffer, "\n")] = 0;

            declare_var(var, tokens.content[2], buffer);
            free(question);
            free(varnameMalloc);
            free(declaration);
            return;
        }

        strcpy(varnameMalloc, declaration);
        char* varname = removeUselessSpace(strtok(varnameMalloc, "="));

        if(strcmp(declaration, varname) == 0 && tokens.length == 2) {
            printf("\n[-]Syntax error on the line:\n%s\nNo value was given to the variable!\n\n", completeLine);
            free(keyword);
            free(completeLine);
            free(varname);
            free(declaration);
            exit(1);
        }

        if(spaceIn(varname)) {
            printf("\n[-]Syntax error on the line:\n%s\n A varname cannot have spaces !\n\n", completeLine);
            free(keyword);
            free(varname);
            free(completeLine);
            free(declaration);
            exit(1);
        }

        strtok(declaration, "=");
        char* tok = strtok(NULL, "=");
        char* content = removeUselessSpace(tok);

        declare_var(var, varname, content);
        free(question);
    }

    else if(strcmp(keyword, "rem/") == 0 || strcmp(keyword, "//") == 0 || strcmp(keyword, "\'") == 0) {
        free(completeLine);
        free(keyword);
        return;
    }

    else {
        printf("\n[-] Syntax error: Keyword %s is unknown\n%s\n\n", keyword, completeLine);
        free(completeLine);
        free(keyword);
        exit(1);
    }

    free(completeLine);
    free(keyword);
}

CPCVAR parse_withvar(CPCLINE lines, char* filename) {
    CPCVAR var;

    init_CPCVAR(&var);
    declare_var(&var, "_EXE_PATH_", filename);

    for(int i=0; i<lines.length; i++) {
        CPCTOKEN tokens = tokenize(lines.content[i]);
        execute(tokens, &var);

        free(tokens.content);
    }

    return var;
}

void parse(CPCLINE line, char* filename) {
    CPCVAR var = parse_withvar(line, filename);
    destroy_CPCVAR(&var);
}

