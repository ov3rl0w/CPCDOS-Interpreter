---
name: Bug report
about: Create a report to help us improve
title: "[ BUG ]"
labels: bug
assignees: 0v3rl0w

---

**Describe the bug**
A clear and concise description of what the bug is.

**To Reproduce**
Provide the .CPC that made the bug

**Expected behavior**
A clear and concise description of what you expected to happen.

**Screenshots**
If applicable, add screenshots to help explain your problem.

**Desktop (please complete the following information):**
 - OS: [e.g. Windows 10]

**Additional context**
Add any other context about the problem here.
