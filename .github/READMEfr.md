# CPC Interpreter


![LICENSE](https://img.shields.io/github/license/0v3rl0w/CPCDOS-Interpreter)
![COMMIT](https://img.shields.io/github/last-commit/0v3rl0w/CPCDOS-Interpreter)

![BUILD](https://img.shields.io/travis/0v3rl0w/CPCDOS-Interpreter)
![SIZE](https://img.shields.io/github/repo-size/0v3rl0w/CPCDOS-Interpreter)


## Infos
### Mais qu'est que ?

Un petit interpréteur fait maison pour simplifier le test de vos scripts CPCDOSC+

### Compiler le projet

Lancez la commande `cmake` dans un dossier build séparer, ensuite `make -j<nombre_de_coeur>`
La sortie binaire sera à la racine de votre dossier build.

### Utilisation

Utilistion: `cpc script` en ligne de commandes
- Où `script` est un chemain absolu ou relatif vers le script .cpc

### Mais que le CPCDOSC+ ?

Le CPCDOSC+ est un langage de programmation visant à la création d'un OS grâce au projet CPCDOS. 
Pour avoir plus d'info je vous invite à:
- [Le site web du projet](https://cpcdos.net)
- [Le Discord la communauté CPCDOS](https://discord.gg/tMA8FeS)
